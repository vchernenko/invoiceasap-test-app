InvoiceASAP Test application
===================

Application is an assessment task to show skills of Android development and architecture.


Project is based on MVVM pattern with Rx and Room in data repository. 

Some features:

GPS location is being monitored in the background of the app, and the user's location is being tracked any time the phone is on, regardless of whether the user is viewing the app or not.

Every 3 minutes in foreground and 20 minutes in background, the local SQLite database is being updated  with the user's current location.  The 3 most recent locations are stored locally.

Each time the location is stored locally, this location is sent to an external API to be recorded remotely.

Some data are prefilled in database and populated in the list using Epoxy.


Features to be implemented
-----------------------------------
* Implement Clean Architecture with Use cases and Mappers
* Add end-to-end (Robolectric), unit and some UI tests
* Separate base functionality in its own Gradle and Dagger module
* Add some dagger's scopes: PerAcitivty, PerFeature and PerView

Features and libraries
-----------------------------------
* [AndroidX libraries](https://developer.android.com/jetpack/androidx)
* [Dagger2](https://github.com/google/dagger)
* [Retrofit2](https://square.github.io/retrofit/)
* [RxJava](https://github.com/ReactiveX/RxJava) / [RxAndroid](https://github.com/ReactiveX/RxAndroid) / [RxBinding](https://github.com/JakeWharton/RxBinding)
* [Room](https://developer.android.com/topic/libraries/architecture/room)
* [WorkManager](https://developer.android.com/topic/libraries/architecture/workmanager)
* [Epoxy](https://github.com/airbnb/epoxy)
* [RxPermissions](https://github.com/tbruyelle/RxPermissions)
* [Navigation Component](https://developer.android.com/guide/navigation/navigation-getting-started)



Developed By
============

* Vitalii Chernenko - <chernenkovit@gmail.com>

