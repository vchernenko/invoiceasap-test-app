package com.chernenkovit.invoiceasaptestapp

import android.app.Application
import com.chernenkovit.invoiceasaptestapp.base.presentation.extension.lazyUnsychronized
import com.chernenkovit.invoiceasaptestapp.presentation.di.component.AppComponent
import com.chernenkovit.invoiceasaptestapp.presentation.di.component.DaggerAppComponent
import com.chernenkovit.invoiceasaptestapp.presentation.di.module.ContextModule
import com.facebook.stetho.Stetho

class App : Application() {

    val appComponent: AppComponent by lazyUnsychronized {
        DaggerAppComponent
                .builder()
                .contextModule(ContextModule(this))
                .build()
    }

    override fun onCreate() {
        super.onCreate()
        injectMembers()
        initStetho()
    }

    private fun injectMembers() = appComponent.inject(this)

    private fun initStetho() {
        val initializerBuilder = Stetho.newInitializerBuilder(this)
        initializerBuilder.enableWebKitInspector(
                Stetho.defaultInspectorModulesProvider(this))
        initializerBuilder.enableDumpapp(
                Stetho.defaultDumperPluginsProvider(this))
        val initializer = initializerBuilder.build()
        Stetho.initialize(initializer)
    }

}