package com.chernenkovit.invoiceasaptestapp.base

import android.util.Log
import com.chernenkovit.invoiceasaptestapp.BuildConfig

object DeveloperUtils {

    fun log(tag: String, message: String) {
        if (BuildConfig.DEBUG) Log.e(tag, message)
    }

    fun log(throwable: Throwable) {
        if (BuildConfig.DEBUG) throwable.printStackTrace()
    }
}