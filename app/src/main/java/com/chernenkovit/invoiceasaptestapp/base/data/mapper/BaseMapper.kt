package com.chernenkovit.invoiceasaptestapp.base.data.mapper

import java.util.*

abstract class BaseMapper<From, To> {

  abstract fun map(from: From): To

  @JvmOverloads
  fun map(froms: List<From>, transformation: ((to: To) -> Unit)? = null): List<To> {
    val result = ArrayList<To>(froms.size)
    for (from in froms) {
      val element = map(from).apply {
        transformation?.invoke(this)
      }
      result.add(element)
    }
    return result
  }
}