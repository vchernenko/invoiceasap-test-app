package com.chernenkovit.invoiceasaptestapp.base.data.mapper

import java.util.*

abstract class Mapper<From, To> : BaseMapper<From, To>() {

  open fun reverse(to: To): From {
    throw NotImplementedError("Override this method in your mapper if needed")
  }

  fun reverse(tos: List<To>): List<From> {
    val result = ArrayList<From>(tos.size)
    for (to in tos) {
      result.add(reverse(to))
    }
    return result
  }
}
