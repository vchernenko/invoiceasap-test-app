package com.chernenkovit.invoiceasaptestapp.base.presentation

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    private val serverFormatter = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    private val uiFormatter = SimpleDateFormat("MM/dd/yy", Locale.ENGLISH)

    fun getTimestampFromServerFormat(dateString: String): Long {
        if (dateString.isEmpty()) return 0
        val date = serverFormatter.parse(dateString) ?: return 0
        return date.time
    }

    fun formatServerStringDateToUiStringDate(dateString: String): String {
        if (dateString.isEmpty()) return ""
        val date = serverFormatter.parse(dateString) ?: return ""
        return uiFormatter.format(date)
    }
}