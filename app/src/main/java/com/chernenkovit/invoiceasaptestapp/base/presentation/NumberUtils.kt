package com.chernenkovit.invoiceasaptestapp.base.presentation

import java.text.NumberFormat
import java.util.*

object NumberUtils {

    fun formatToCurrency(amount: Long): String {
        val formatter = NumberFormat.getCurrencyInstance(Locale.US)
        return formatter.format(amount / 100.0)
    }
}