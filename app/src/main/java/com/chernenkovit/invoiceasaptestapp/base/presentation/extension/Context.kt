package com.chernenkovit.invoiceasaptestapp.base.presentation.extension

import android.content.Context
import android.net.ConnectivityManager
import android.view.inputmethod.InputMethodManager

val Context.isConnected: Boolean
    get() {
        return (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
            .activeNetworkInfo?.isConnected == true
    }

val Context.inputMethodManager: InputMethodManager
    get() =
        (this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
