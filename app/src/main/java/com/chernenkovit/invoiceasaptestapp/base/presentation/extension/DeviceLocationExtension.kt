package com.chernenkovit.invoiceasaptestapp.base.presentation.extension

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Build
import android.provider.Settings
import androidx.core.content.ContextCompat

fun Context.isGrantedLocationPermission(): Boolean {
    return ContextCompat.checkSelfPermission(
        this,
        Manifest.permission.ACCESS_FINE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED
}

fun Context.isGrantedBackgroundLocationPermission(): Boolean {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }
    return true
}

fun Context.isEnabledGPS(): Boolean {
    val locationMode = getLocationMode()

    if (locationMode != -1 && locationMode != Settings.Secure.LOCATION_MODE_OFF) {
        return true
    }

    var isDisableGPS = false
    val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

    if (locationManager != null) {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            isDisableGPS = true
        }

        if (!isDisableGPS) {
            val providerString = Settings.Secure.getString(
                contentResolver,
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED
            )

            if (providerString.isEmpty()) {
                isDisableGPS = true
            }
        }
    }

    if (!isDisableGPS) {
        try {
            val providerMode = Settings.Secure.getInt(
                contentResolver,
                Settings.Secure.LOCATION_MODE
            )

            if (providerMode == 0) {
                isDisableGPS = true
            }
        } catch (ignored: Settings.SettingNotFoundException) {
        }

    }

    return !isDisableGPS
}

fun Context.getLocationMode(): Int {
    try {
        return Settings.Secure.getInt(contentResolver, Settings.Secure.LOCATION_MODE)
    } catch (ignored: Settings.SettingNotFoundException) {
    }

    return -1
}