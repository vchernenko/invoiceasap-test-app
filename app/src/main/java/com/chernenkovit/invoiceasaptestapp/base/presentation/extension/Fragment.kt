package com.chernenkovit.invoiceasaptestapp.base.presentation.extension

import android.content.Context
import android.view.View
import com.chernenkovit.invoiceasaptestapp.base.presentation.ui.BaseFragment
import com.chernenkovit.invoiceasaptestapp.presentation.MainActivity
import kotlinx.android.synthetic.main.main_activity.*

val BaseFragment.viewContainer: View get() = (activity as MainActivity).container

val BaseFragment.appContext: Context get() = activity?.applicationContext!!