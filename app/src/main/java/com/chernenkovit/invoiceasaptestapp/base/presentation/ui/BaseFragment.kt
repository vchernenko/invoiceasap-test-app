package com.chernenkovit.invoiceasaptestapp.base.presentation.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.chernenkovit.invoiceasaptestapp.App
import com.chernenkovit.invoiceasaptestapp.base.presentation.extension.lazyUnsychronized
import com.chernenkovit.invoiceasaptestapp.presentation.di.component.AppComponent
import javax.inject.Inject

/**
 * Base Fragment class with helper methods for handling views and back button events.
 *
 * @see Fragment
 */
abstract class BaseFragment : Fragment() {

    abstract fun layoutId(): Int

    val appComponent: AppComponent by lazyUnsychronized {
        (activity?.application as App).appComponent
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater.inflate(layoutId(), container, false)
}
