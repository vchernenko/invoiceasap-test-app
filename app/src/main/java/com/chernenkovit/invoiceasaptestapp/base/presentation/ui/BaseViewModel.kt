package com.chernenkovit.invoiceasaptestapp.base.presentation.ui

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

/**
 * Base ViewModel class.
 * @see ViewModel
 */
abstract class BaseViewModel : ViewModel() {

    val subscriptions: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        subscriptions.clear()
    }
}