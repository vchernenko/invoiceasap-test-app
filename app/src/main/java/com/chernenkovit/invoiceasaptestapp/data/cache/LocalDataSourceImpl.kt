package com.chernenkovit.invoiceasaptestapp.data.cache

import com.chernenkovit.invoiceasaptestapp.data.cache.db.InvoiceAsapDatabase
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerEntity
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerWithInvoices
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.InvoiceEntity
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.LocationEntity
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Local data source implementation
 */
class LocalDataSourceImpl @Inject constructor(
        private val database: InvoiceAsapDatabase) {

    fun insertCustomers(customers: List<CustomerEntity>): Completable {
        return database.customerDao().insert(customers)
    }

    fun insertInvoices(invoices: List<InvoiceEntity>): Completable {
        return database.invoiceDao().insert(invoices)
    }

    fun getCustomersWithInvoices(): Single<List<CustomerWithInvoices>> {
        return database.invoiceDao().getCustomersWithInvoices()
    }

    fun getLastLocation(): Single<List<LocationEntity>> {
        return database.locationDao().getLastLocation()
    }

    fun insertLocation(locationEntity: LocationEntity): Completable {
        return database.locationDao().insert(locationEntity)
    }

    fun trimLocationsQuantity(): Completable {
        return database.locationDao().trimLocationsQuantity()
    }
}