package com.chernenkovit.invoiceasaptestapp.data.cache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerEntity
import io.reactivex.Completable

@Dao
interface CustomerDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(customers: List<CustomerEntity>): Completable
}
