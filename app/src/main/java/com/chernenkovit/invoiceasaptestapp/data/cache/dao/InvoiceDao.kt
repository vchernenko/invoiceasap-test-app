package com.chernenkovit.invoiceasaptestapp.data.cache.dao

import androidx.room.*
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerWithInvoices
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.InvoiceEntity
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface InvoiceDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(invoices: List<InvoiceEntity>): Completable

    @Transaction
    @Query("SELECT * FROM CUSTOMERS")
    fun getCustomersWithInvoices(): Single<List<CustomerWithInvoices>>
}
