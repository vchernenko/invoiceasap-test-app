package com.chernenkovit.invoiceasaptestapp.data.cache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.LocationEntity
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface LocationDao {

    @Insert
    fun insert(location: LocationEntity): Completable

    @Query(value = "DELETE FROM Location WHERE id NOT IN (SELECT id FROM Location ORDER BY id DESC LIMIT 3)")
    fun trimLocationsQuantity(): Completable

    @Query(value = "SELECT * FROM Location ORDER BY location_timestamp DESC LIMIT 1")
    fun getLastLocation(): Single<List<LocationEntity>>

}