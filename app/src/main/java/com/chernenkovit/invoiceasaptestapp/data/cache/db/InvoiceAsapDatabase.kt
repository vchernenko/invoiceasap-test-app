package com.chernenkovit.invoiceasaptestapp.data.cache.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.chernenkovit.invoiceasaptestapp.data.cache.dao.CustomerDao
import com.chernenkovit.invoiceasaptestapp.data.cache.dao.InvoiceDao
import com.chernenkovit.invoiceasaptestapp.data.cache.dao.LocationDao
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerEntity
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.InvoiceEntity
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.LocationEntity

@Database(
    entities = [
        CustomerEntity::class, InvoiceEntity::class, LocationEntity::class],
    version = 1, exportSchema = false
)
abstract class InvoiceAsapDatabase : RoomDatabase() {

    abstract fun customerDao(): CustomerDao

    abstract fun invoiceDao(): InvoiceDao

    abstract fun locationDao(): LocationDao

    companion object {
        const val DATABASE_NAME = "invoice_asap-db"
    }
}