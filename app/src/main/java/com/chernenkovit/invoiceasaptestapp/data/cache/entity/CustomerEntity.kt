package com.chernenkovit.invoiceasaptestapp.data.cache.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerEntity.Companion.CUSTOMERS_TABLE

@Entity(tableName = CUSTOMERS_TABLE)
data class CustomerEntity(

    @ColumnInfo(name = ID_COLUMN)
    @PrimaryKey
    var id: String,

    @ColumnInfo(name = NAME_COLUMN)
    var name: String
) {

    companion object {
        const val CUSTOMERS_TABLE = "customers"
        const val ID_COLUMN = "id"
        const val NAME_COLUMN = "name"
    }
}