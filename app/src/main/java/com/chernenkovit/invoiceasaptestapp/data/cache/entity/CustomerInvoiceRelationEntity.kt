package com.chernenkovit.invoiceasaptestapp.data.cache.entity

import androidx.room.*
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerInvoiceRelationEntity.Companion.CUSTOMER_ID_COLUMN
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerInvoiceRelationEntity.Companion.CUSTOMER_INVOICE_RELATION_TABLE
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerInvoiceRelationEntity.Companion.INVOICE_ID_COLUMN

/**
 * InvoiceEntity can be refactored in this relations table for better logic separation, unused for now
 */
@Entity(
    tableName = CUSTOMER_INVOICE_RELATION_TABLE,
    foreignKeys = [(ForeignKey(
        entity = CustomerEntity::class,
        parentColumns = arrayOf(CustomerEntity.ID_COLUMN),
        childColumns = arrayOf(CUSTOMER_ID_COLUMN),
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.CASCADE
    )),
        (ForeignKey(
            entity = InvoiceEntity::class,
            parentColumns = arrayOf(InvoiceEntity.ID_COLUMN),
            childColumns = arrayOf(INVOICE_ID_COLUMN),
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ))],
    indices = [(Index(value = [CUSTOMER_ID_COLUMN, INVOICE_ID_COLUMN], unique = true))]
)
data class CustomerInvoiceRelationEntity(
    @ColumnInfo(name = CUSTOMER_ID_COLUMN)
    val artistId: String,

    @ColumnInfo(name = INVOICE_ID_COLUMN)
    val trackId: String
) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = ID_COLUMN)
    var id: Long = 0

    companion object {
        const val CUSTOMER_INVOICE_RELATION_TABLE = "customer_invoice_relation"
        const val ID_COLUMN = "id"
        const val CUSTOMER_ID_COLUMN = "customer_id"
        const val INVOICE_ID_COLUMN = "invoice_id"
    }
}