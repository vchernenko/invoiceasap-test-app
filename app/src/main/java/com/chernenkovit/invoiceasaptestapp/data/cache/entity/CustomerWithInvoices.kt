package com.chernenkovit.invoiceasaptestapp.data.cache.entity

import androidx.room.Embedded
import androidx.room.Relation

/**
 * One-to-many (1 customer->multiple invoices) data class
 */
data class CustomerWithInvoices(

    @Embedded
    val customer: CustomerEntity,

    @Relation(
        parentColumn = CustomerEntity.ID_COLUMN,
        entityColumn = InvoiceEntity.CUSTOMER_ID_COLUMN)
    val invoices: List<InvoiceEntity>
)