package com.chernenkovit.invoiceasaptestapp.data.cache.entity

import androidx.annotation.StringDef
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.InvoiceEntity.Companion.INVOICES_TABLE

@Entity(tableName = INVOICES_TABLE)
data class InvoiceEntity(

    @ColumnInfo(name = ID_COLUMN)
    @PrimaryKey
    val id: String,

    @ColumnInfo(name = CUSTOMER_ID_COLUMN)
    val customerId: String,

    @ColumnInfo(name = NUMBER_COLUMN)
    val number: String,

    @ColumnInfo(name = SENT_STATUS_COLUMN)
    val sentStatus: String? = "",

    @ColumnInfo(name = DUE_DATE_COLUMN)
    val dueDate: String,

    @ColumnInfo(name = BALANCE_COLUMN)
    val balance: Long,

    @ColumnInfo(name = PAYMENT_STATUS_COLUMN)
    val paymentStatus: String? = ""
) {

    class SentStatusClass {
        companion object {
            @StringDef(
                OPENED,
                VIEWED
            )
            @Retention(AnnotationRetention.SOURCE)
            annotation class SentStatusClass

            const val OPENED = "Opened"
            const val VIEWED = "Viewed"
        }
    }

    class PaymentStatusClass {
        companion object {
            @StringDef(
                LATE,
                OPEN,
                PAID
            )
            @Retention(AnnotationRetention.SOURCE)
            annotation class SentStatusClass

            const val LATE = "Late"
            const val OPEN = "Open"
            const val PAID = "Paid"
        }
    }

    companion object {
        const val INVOICES_TABLE = "invoices"
        const val ID_COLUMN = "id"
        const val CUSTOMER_ID_COLUMN = "customer_id"
        const val NUMBER_COLUMN = "number"
        const val SENT_STATUS_COLUMN = "sent_status"
        const val DUE_DATE_COLUMN = "due_date"
        const val BALANCE_COLUMN = "balance"
        const val PAYMENT_STATUS_COLUMN = "payment_status"
    }
}