package com.chernenkovit.invoiceasaptestapp.data.cache.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = LocationEntity.LOCATION_TABLE)
data class LocationEntity(

    @ColumnInfo(name = LAT_COLUMN)
    val latitude: Double,

    @ColumnInfo(name = LONGITUDE_COLUMN)
    val longitude: Double,

    @ColumnInfo(name = ACCURACY_COLUMN)
    val accuracy: Float,

    @ColumnInfo(name = LOCATION_TIMESTAMP_COLUMN)
    val timestamp: Long
) {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    companion object {
        const val LOCATION_TABLE = "location"
        const val LAT_COLUMN = "lat"
        const val LONGITUDE_COLUMN = "longitude"
        const val ACCURACY_COLUMN = "accuracy"
        const val LOCATION_TIMESTAMP_COLUMN = "location_timestamp"
    }
}