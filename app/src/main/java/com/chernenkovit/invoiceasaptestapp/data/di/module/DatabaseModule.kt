package com.chernenkovit.invoiceasaptestapp.data.di.module

import android.content.Context
import androidx.room.Room
import com.chernenkovit.invoiceasaptestapp.data.cache.db.InvoiceAsapDatabase
import com.chernenkovit.invoiceasaptestapp.data.cache.db.InvoiceAsapDatabase.Companion.DATABASE_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class DatabaseModule {

    @Module
    companion object {

        @JvmStatic
        @Provides
        @Singleton
        fun provideRoomDatabase(context: Context): InvoiceAsapDatabase =
            Room.databaseBuilder(
                context,
                InvoiceAsapDatabase::class.java,
                DATABASE_NAME
            ).fallbackToDestructiveMigration().build()
    }
}
