package com.chernenkovit.invoiceasaptestapp.data.di.module

import com.chernenkovit.invoiceasaptestapp.data.executor.JobExecutor
import com.chernenkovit.invoiceasaptestapp.data.executor.PostExecutionThread
import com.chernenkovit.invoiceasaptestapp.data.executor.ThreadExecutor
import com.chernenkovit.invoiceasaptestapp.data.executor.UiThread
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Executors to replace Rx Schedulers
 */
@Module
interface ExecutorsModule {

    @Singleton
    @Binds
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor

    @Binds
    @Singleton
    fun providePostExecutionThread(uiThread: UiThread): PostExecutionThread
}
