package com.chernenkovit.invoiceasaptestapp.data.di.module

import com.chernenkovit.invoiceasaptestapp.data.location.common.LocationProvider
import com.chernenkovit.invoiceasaptestapp.data.location.factory.LocationProviderFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class LocationModule {

    @Module
    companion object {
        @JvmStatic
        @Singleton
        @Provides
        fun provideLocationProvider(locationProviderFactory: LocationProviderFactory): LocationProvider =
            locationProviderFactory.locationProvider
    }

}