package com.chernenkovit.invoiceasaptestapp.data.di.module

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
abstract class NetworkModule {

    @Module
    companion object {
        @JvmStatic
        @Singleton
        @Provides
        fun provideRetrofit(
            httpClient: OkHttpClient,
            gsonConverterFactory: GsonConverterFactory
        ): Retrofit {
            val retrofitBuilder = Retrofit.Builder()
                .baseUrl("https://google.com/") //BuildConfig.BASE_URL usually
                .addConverterFactory(gsonConverterFactory)
                .client(httpClient)
            return retrofitBuilder.build()
        }

        @JvmStatic
        @Provides
        @Singleton
        fun provideGsonConverterFactory(): GsonConverterFactory {
            val gson = GsonBuilder()
                .create()
            return GsonConverterFactory.create(gson)
        }

        @JvmStatic
        @Singleton
        @Provides
        fun provideHttpClient(): OkHttpClient {
            val builder = OkHttpClient.Builder().readTimeout(30, TimeUnit.SECONDS)
                .addNetworkInterceptor(StethoInterceptor())
            return builder.build()
        }
    }
}