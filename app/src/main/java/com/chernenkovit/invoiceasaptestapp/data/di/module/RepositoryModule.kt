package com.chernenkovit.invoiceasaptestapp.data.di.module

import com.chernenkovit.invoiceasaptestapp.data.repository.RepositoryImpl
import com.chernenkovit.invoiceasaptestapp.domain.Repository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface RepositoryModule {

    @Binds
    @Singleton
    fun provideRepository(repository: RepositoryImpl): Repository
}