package com.chernenkovit.invoiceasaptestapp.data.location

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.chernenkovit.invoiceasaptestapp.data.location.utils.WorkSchedulerUtils

class BootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (Intent.ACTION_BOOT_COMPLETED == intent?.action) {
            WorkSchedulerUtils.scheduleLocationWork()
        }
    }
}