package com.chernenkovit.invoiceasaptestapp.data.location.common

import android.content.Context
import android.location.Location
import androidx.annotation.CallSuper
import com.chernenkovit.invoiceasaptestapp.base.DeveloperUtils.log
import com.chernenkovit.invoiceasaptestapp.base.presentation.extension.isGrantedLocationPermission

abstract class AbstractLocationProvider protected constructor(
    protected val context: Context) : LocationProvider {

  private var locationServiceCallback: LocationCallback? = null
  private val isGrantedPermission: Boolean
    get() = context.isGrantedLocationPermission()

  protected abstract fun requestLocationUpdates()

  protected abstract fun removeLocationUpdates()

  @CallSuper
  @Synchronized
  override fun start() {
      startLocationUpdates()
  }

  @CallSuper
  @Synchronized
  override fun stop() {
    stopLocationUpdates()
  }

  @CallSuper
  override fun setLocationServiceCallback(locationCallback: LocationCallback) {
    this.locationServiceCallback = locationCallback
  }

  protected fun onLocationChanged(location: Location) {
    locationServiceCallback?.onLocationChanged(location)
  }

  private fun startLocationUpdates() {
    stopLocationUpdates()
    if (!isGrantedPermission) {
      log(SecurityException("Location permissions not granted"))
      return
    }
    requestLocationUpdates()
  }

  private fun stopLocationUpdates() {
    try {
      removeLocationUpdates()
    } catch (exception: Exception) {
      log(exception)
    }
  }
}
