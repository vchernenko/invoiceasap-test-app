package com.chernenkovit.invoiceasaptestapp.data.location.common

import android.location.Location

/**
 * Implement this callback when you need to subscibe to location updates (background or foreground)
 */
interface LocationCallback {

    fun onLocationChanged(location: Location)
}