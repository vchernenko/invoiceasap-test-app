package com.chernenkovit.invoiceasaptestapp.data.location.common


interface LocationProvider {

  fun start()

  fun stop()

  fun setLocationServiceCallback(locationCallback: LocationCallback)
}
