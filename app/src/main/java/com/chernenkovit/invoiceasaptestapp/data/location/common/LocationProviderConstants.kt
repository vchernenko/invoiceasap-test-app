package com.chernenkovit.invoiceasaptestapp.data.location.common

import java.util.concurrent.TimeUnit

object LocationProviderConstants {
    val UPDATE_INTERVAL_IN_MILLISECONDS = TimeUnit.MINUTES.toMillis(3)
    val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = TimeUnit.MINUTES.toMillis(3)
    const val UPDATE_DISTANCE_METERS = 0F
}