package com.chernenkovit.invoiceasaptestapp.data.location.factory

import com.chernenkovit.invoiceasaptestapp.data.location.location_provider.LocationManagerLocationProvider
import com.chernenkovit.invoiceasaptestapp.data.location.location_provider.PlayServiceLocationProvider
import com.chernenkovit.invoiceasaptestapp.data.location.common.LocationProvider
import dagger.Lazy
import javax.inject.Inject

/**
 * Factory for location providers
 */
class LocationProviderFactory @Inject constructor(
    private val playServiceAvailableHelper: PlayServiceAvailableHelper,
    private val playServiceLocationProvider: Lazy<PlayServiceLocationProvider>,
    private val locationManagerLocationProvider: Lazy<LocationManagerLocationProvider>) {

  val locationProvider: LocationProvider
    get() {
      return if (playServiceAvailableHelper.isGooglePlayServicesAvailable) {
        playServiceLocationProvider.get()
      } else {
        locationManagerLocationProvider.get()
      }
    }

}
