package com.chernenkovit.invoiceasaptestapp.data.location.factory

import android.content.Context
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import javax.inject.Inject

class PlayServiceAvailableHelper @Inject constructor(
    private val context: Context) {

  val isGooglePlayServicesAvailable: Boolean by lazy {
    isPlayServicesAvailable()
  }

  private fun isPlayServicesAvailable(): Boolean {
    val googleApiAvailability = GoogleApiAvailability.getInstance()
    val status = googleApiAvailability.isGooglePlayServicesAvailable(context)
    return status == ConnectionResult.SUCCESS
  }

}