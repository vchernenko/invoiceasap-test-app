package com.chernenkovit.invoiceasaptestapp.data.location.location_provider

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import com.chernenkovit.invoiceasaptestapp.data.location.common.AbstractLocationProvider
import com.chernenkovit.invoiceasaptestapp.data.location.common.LocationProviderConstants.UPDATE_DISTANCE_METERS
import com.chernenkovit.invoiceasaptestapp.data.location.common.LocationProviderConstants.UPDATE_INTERVAL_IN_MILLISECONDS
import javax.inject.Inject

class LocationManagerLocationProvider @Inject constructor(
    context: Context) : AbstractLocationProvider(context) {

  private val locationManager: LocationManager by lazy {
    initLocationManager()
  }

  private val locationListener: LocationListener by lazy { initLocationListener() }

  @SuppressLint("MissingPermission")
  override fun requestLocationUpdates() {
    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, UPDATE_INTERVAL_IN_MILLISECONDS, UPDATE_DISTANCE_METERS,
        locationListener)
  }

  override fun removeLocationUpdates() {
    locationManager.removeUpdates(locationListener)
  }

  private fun initLocationManager(): LocationManager {
    return context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
  }

  private fun initLocationListener(): LocationListener {
    return object : LocationListener {
      override fun onLocationChanged(location: Location?) {
        handleLocationChanged(location)
      }

      override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}

      override fun onProviderEnabled(provider: String) {}

      override fun onProviderDisabled(provider: String) {}
    }
  }

  private fun handleLocationChanged(location: Location?) {
    if (location == null) {
      return
    }
    onLocationChanged(location)
  }
}
