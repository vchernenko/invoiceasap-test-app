package com.chernenkovit.invoiceasaptestapp.data.location.location_provider

import android.content.Context
import android.os.Looper
import com.chernenkovit.invoiceasaptestapp.base.DeveloperUtils.log
import com.chernenkovit.invoiceasaptestapp.data.location.common.AbstractLocationProvider
import com.chernenkovit.invoiceasaptestapp.data.location.common.LocationProviderConstants.FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
import com.chernenkovit.invoiceasaptestapp.data.location.common.LocationProviderConstants.UPDATE_DISTANCE_METERS
import com.chernenkovit.invoiceasaptestapp.data.location.common.LocationProviderConstants.UPDATE_INTERVAL_IN_MILLISECONDS
import com.google.android.gms.location.*
import javax.inject.Inject

class PlayServiceLocationProvider @Inject constructor(
    context: Context) : AbstractLocationProvider(context) {

  private val locationRequest: LocationRequest by lazy {
    initForceLocationRequest()
  }

  private val fusedLocationProviderClient: FusedLocationProviderClient by lazy {
    initLocationClient()
  }

  private val locationCallback: LocationCallback by lazy { initLocationCallback() }

  override fun requestLocationUpdates() {
    checkLocationAvailability()
    fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback,
        Looper.getMainLooper())
  }

  override fun removeLocationUpdates() {
    fusedLocationProviderClient.removeLocationUpdates(locationCallback)
  }

  private fun initLocationClient(): FusedLocationProviderClient {
    return LocationServices.getFusedLocationProviderClient(context)
  }

  private fun initLocationCallback(): LocationCallback {
    return object : LocationCallback() {
      override fun onLocationResult(locationResult: LocationResult?) {
        if (locationResult == null) {
          return
        }

        onLocationCallbackResult(locationResult)
      }

      override fun onLocationAvailability(locationAvailability: LocationAvailability?) {
        onLocationCallbackAvailability(locationAvailability)
      }
    }
  }

  private fun initForceLocationRequest(): LocationRequest {
    return LocationRequest.create()
        .setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
        .setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS)
        .setSmallestDisplacement(UPDATE_DISTANCE_METERS)
        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
  }

  private fun onLocationCallbackResult(locationResult: LocationResult) {
    onLocationChanged(locationResult.lastLocation)
  }

  private fun checkLocationAvailability() {
    fusedLocationProviderClient.locationAvailability
        .addOnSuccessListener { onLocationCallbackAvailability(it) }
        .addOnFailureListener { checkLocationAvailabilityFailure(it) }
  }

  private fun onLocationCallbackAvailability(locationAvailability: LocationAvailability?) {
    log("Location - ","onLocationCallbackAvailability() called with: availability = [$locationAvailability]")
  }

  private fun checkLocationAvailabilityFailure(exception: Exception) {
    log(exception)
  }

}
