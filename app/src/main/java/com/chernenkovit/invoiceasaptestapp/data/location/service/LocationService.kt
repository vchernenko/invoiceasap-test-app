package com.chernenkovit.invoiceasaptestapp.data.location.service

import android.app.Service
import android.content.Intent
import android.location.Location
import android.os.Binder
import android.os.IBinder
import com.chernenkovit.invoiceasaptestapp.App
import com.chernenkovit.invoiceasaptestapp.base.DeveloperUtils.log
import com.chernenkovit.invoiceasaptestapp.data.executor.ThreadExecutor
import com.chernenkovit.invoiceasaptestapp.data.location.common.LocationCallback
import com.chernenkovit.invoiceasaptestapp.data.location.common.LocationProvider
import com.chernenkovit.invoiceasaptestapp.domain.Repository
import com.chernenkovit.invoiceasaptestapp.presentation.di.component.AppComponent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Base location service that operates with location providers and getting callbacks
 * Works only if app is visible. In case of app killed on not started - {@link PeriodicLocationWorker}
 */
class LocationService : Service() {

    @Inject
    lateinit var locationProvider: LocationProvider

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var threadExecutor: ThreadExecutor

    private val appComponent: AppComponent by lazy(mode = LazyThreadSafetyMode.NONE) {
        (application as App).appComponent
    }

    private val subscriptions = CompositeDisposable()
    private val binder = LocalBinder()

    inner class LocalBinder : Binder() {
        fun getService(): LocationService {
            return this@LocationService
        }
    }

    private val localLocationServiceCallback = object : LocationCallback {
        override fun onLocationChanged(location: Location) {
            log("Location was updated from foreground - ", location.toString())
            onLocationDefined(location)
        }
    }

    private fun onLocationDefined(location: Location) {
        subscriptions.add(repository.saveLocation(location)
                     .subscribeOn(Schedulers.io())
                     .observeOn(Schedulers.io())
                     .subscribe({},
                         { log(it) })
        )
    }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
        locationProvider.setLocationServiceCallback(localLocationServiceCallback)
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        locationProvider.start()
        return binder
    }

    override fun onUnbind(intent: Intent): Boolean {
        locationProvider.stop()
        return super.onUnbind(intent)
    }

    override fun onDestroy() {
        locationProvider.stop()
        subscriptions.dispose()
        super.onDestroy()
    }
}
