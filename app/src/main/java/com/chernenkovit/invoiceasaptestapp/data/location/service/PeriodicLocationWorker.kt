package com.chernenkovit.invoiceasaptestapp.data.location.service

import android.content.Context
import android.location.Location
import androidx.concurrent.futures.CallbackToFutureAdapter
import androidx.work.ListenableWorker
import androidx.work.WorkerParameters
import com.chernenkovit.invoiceasaptestapp.App
import com.chernenkovit.invoiceasaptestapp.base.DeveloperUtils.log
import com.chernenkovit.invoiceasaptestapp.base.presentation.extension.isEnabledGPS
import com.chernenkovit.invoiceasaptestapp.base.presentation.extension.isGrantedBackgroundLocationPermission
import com.chernenkovit.invoiceasaptestapp.base.presentation.extension.isGrantedLocationPermission
import com.chernenkovit.invoiceasaptestapp.data.executor.ThreadExecutor
import com.chernenkovit.invoiceasaptestapp.data.location.common.LocationCallback
import com.chernenkovit.invoiceasaptestapp.data.location.common.LocationProvider
import com.chernenkovit.invoiceasaptestapp.domain.Repository
import com.google.common.util.concurrent.ListenableFuture
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Periodic worker for tracking user location when app is in background, killed or not started
 */
class PeriodicLocationWorker(val context: Context, params: WorkerParameters) : ListenableWorker(
    context, params
) {

    @Inject
    lateinit var repository: Repository

    @Inject
    lateinit var locationProvider: LocationProvider

    @Inject
    lateinit var threadExecutor: ThreadExecutor

    private val subscriptions = CompositeDisposable()

    override fun startWork(): ListenableFuture<Result> {
        injectDependencies()

        return CallbackToFutureAdapter.getFuture { completer ->
            if (!context.isGrantedLocationPermission() || !context.isGrantedBackgroundLocationPermission() || !context.isEnabledGPS()) {
                completer.set(Result.failure())
            } else {
                locationProvider.setLocationServiceCallback(object : LocationCallback {
                    override fun onLocationChanged(location: Location) {
                        onLocationDefined(location, completer)
                    }
                })
                locationProvider.start()
            }
        }
    }

    override fun onStopped() {
        subscriptions.clear()
    }

    private fun onLocationDefined(
        location: Location,
        completer: CallbackToFutureAdapter.Completer<Result>
    ) {
        log("Location was updated from background - ", location.toString())
        locationProvider.stop()
        saveLocation(location, completer)
    }

    private fun saveLocation(
        location: Location,
        completer: CallbackToFutureAdapter.Completer<Result>
    ) {
        subscriptions.add(
            repository.saveLocation(location)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe({
                    completer.set(Result.success())
                }, {
                    log(it)
                    completer.set(Result.failure())
                })
        )
    }

    private fun injectDependencies() {
        (context.applicationContext as App).appComponent.inject(this)
    }

    companion object {
        const val UNIQUE_WORKER_NAME = "PERIODIC_LOCATION_WORKER"
    }

}