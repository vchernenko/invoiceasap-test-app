package com.chernenkovit.invoiceasaptestapp.data.location.utils

import androidx.work.ExistingPeriodicWorkPolicy.KEEP
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import com.chernenkovit.invoiceasaptestapp.data.location.service.PeriodicLocationWorker
import java.util.concurrent.TimeUnit

object WorkSchedulerUtils {
    fun scheduleLocationWork() {
        val repeatInterval = 20L
        val periodicWork = PeriodicWorkRequest.Builder(
            PeriodicLocationWorker::class.java, repeatInterval, TimeUnit.MINUTES).build()
        WorkManager.getInstance().enqueueUniquePeriodicWork(
            PeriodicLocationWorker.UNIQUE_WORKER_NAME,
            KEEP, periodicWork
        )
    }

    fun cancelLocationWork() {
        WorkManager.getInstance().cancelUniqueWork(PeriodicLocationWorker.UNIQUE_WORKER_NAME)
    }
}
