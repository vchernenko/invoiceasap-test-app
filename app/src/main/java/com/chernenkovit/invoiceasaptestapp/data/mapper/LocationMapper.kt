package com.chernenkovit.invoiceasaptestapp.data.mapper

import android.location.Location
import com.chernenkovit.invoiceasaptestapp.base.data.mapper.Mapper
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.LocationEntity
import javax.inject.Inject

class LocationMapper @Inject constructor() : Mapper<Location, LocationEntity>() {

    override fun map(from: Location): LocationEntity = from.run {
        LocationEntity(
            from.latitude,
            from.longitude,
            from.accuracy,
            from.time
        )
    }
}
