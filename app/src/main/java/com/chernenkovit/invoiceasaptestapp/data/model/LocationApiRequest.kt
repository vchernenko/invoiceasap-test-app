package com.chernenkovit.invoiceasaptestapp.data.model

import android.location.Location
import androidx.annotation.Keep

@Keep
class LocationApiRequest(val location: Location)