package com.chernenkovit.invoiceasaptestapp.data.network.api

import com.chernenkovit.invoiceasaptestapp.data.model.LocationApiRequest
import io.reactivex.Completable
import retrofit2.http.Body
import retrofit2.http.POST


interface RemoteApiService  {

    @POST("someServiceEndPoint/sendLocation")
    fun sendLocation(@Body request: LocationApiRequest): Completable
}