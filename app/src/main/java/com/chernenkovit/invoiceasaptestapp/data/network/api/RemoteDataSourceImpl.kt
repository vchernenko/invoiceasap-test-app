package com.chernenkovit.invoiceasaptestapp.data.network.api

import android.location.Location
import io.reactivex.Completable
import retrofit2.Retrofit
import javax.inject.Inject

/**
 * Remote data source implementation
 */
class RemoteDataSourceImpl @Inject constructor(
        private val retrofit: Retrofit) {

    private val remoteApiService by lazy { retrofit.create(RemoteApiService::class.java) }


    fun sendLocation(location: Location): Completable {
//        remoteApiService.sendLocation(LocationApiRequest(location))
        return Completable.complete()
    }
}