package com.chernenkovit.invoiceasaptestapp.data.repository

import android.location.Location
import com.chernenkovit.invoiceasaptestapp.data.cache.LocalDataSourceImpl
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerEntity
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerWithInvoices
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.InvoiceEntity
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.LocationEntity
import com.chernenkovit.invoiceasaptestapp.data.mapper.LocationMapper
import com.chernenkovit.invoiceasaptestapp.data.network.api.RemoteDataSourceImpl
import com.chernenkovit.invoiceasaptestapp.domain.Repository
import io.reactivex.Completable
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Base repository implementation for operating with data
 */
class RepositoryImpl @Inject constructor(
    private val localDataSource: LocalDataSourceImpl,
    private val remoteDataSource: RemoteDataSourceImpl,
    private val locationMapper: LocationMapper
) : Repository {

    override fun insertCustomers(customers: List<CustomerEntity>): Completable {
        return localDataSource.insertCustomers(customers)
    }

    override fun insertInvoices(invoices: List<InvoiceEntity>): Completable {
        return localDataSource.insertInvoices(invoices)
    }

    override fun getInvoices(): Single<List<CustomerWithInvoices>> {
        return localDataSource.getCustomersWithInvoices()
    }

    override fun getLastLocation(): Single<List<LocationEntity>> {
        return localDataSource.getLastLocation()
    }

    /**
     * Save and send the new location only if more then 2 minutes passed
     */
    override fun saveLocation(location: Location): Completable {
        return localDataSource
            .getLastLocation()
            .flatMapCompletable {
                if (it.isNotEmpty() && location.time - it[0].timestamp < TimeUnit.MINUTES.toMillis(2)) {
                    return@flatMapCompletable Completable.complete()
                }
                localDataSource.insertLocation(locationMapper.map(location))
                    .andThen(localDataSource.trimLocationsQuantity())
                    .andThen(remoteDataSource.sendLocation(location))
            }
    }
}