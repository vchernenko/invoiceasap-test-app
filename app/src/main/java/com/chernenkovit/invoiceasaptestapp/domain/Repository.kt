package com.chernenkovit.invoiceasaptestapp.domain

import android.location.Location
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerEntity
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerWithInvoices
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.InvoiceEntity
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.LocationEntity
import io.reactivex.Completable
import io.reactivex.Single

interface Repository {

    fun insertCustomers(customers: List<CustomerEntity>): Completable

    fun insertInvoices(invoices: List<InvoiceEntity>): Completable

    fun getInvoices(): Single<List<CustomerWithInvoices>>

    fun getLastLocation(): Single<List<LocationEntity>>

    fun saveLocation(location: Location): Completable
}