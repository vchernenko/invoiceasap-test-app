package com.chernenkovit.invoiceasaptestapp.presentation

import android.Manifest
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.chernenkovit.invoiceasaptestapp.App
import com.chernenkovit.invoiceasaptestapp.R
import com.chernenkovit.invoiceasaptestapp.base.presentation.extension.lazyUnsychronized
import com.chernenkovit.invoiceasaptestapp.data.location.service.LocationService
import com.chernenkovit.invoiceasaptestapp.data.location.utils.WorkSchedulerUtils
import com.chernenkovit.invoiceasaptestapp.presentation.di.component.AppComponent
import com.chernenkovit.invoiceasaptestapp.presentation.ui.MainViewModel
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Single activity container for fragments and base UI or context based functionality
 */
class MainActivity : AppCompatActivity(), ServiceConnection {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val appComponent: AppComponent by lazyUnsychronized {
        (application as App).appComponent
    }

    private val rxPermissions = RxPermissions(this)
    private val subscriptions = CompositeDisposable()
    private lateinit var viewModel: MainViewModel
    private var isServiceConnected = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)
        setContentView(R.layout.main_activity)
        Navigation.findNavController(this, R.id.nav_host_fragment)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        WorkSchedulerUtils.cancelLocationWork()
        checkPermissions()
    }

    private fun checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            subscriptions.add(rxPermissions
                .requestEach(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
                ).subscribe { permission ->
                    if (permission.name == Manifest.permission.ACCESS_FINE_LOCATION && permission.granted) {
                        bindService()
                    }
                })
        }
    }

    private fun bindService() {
        WorkSchedulerUtils.cancelLocationWork()
        bindService(
            Intent(this, LocationService::class.java),
            this,
            Context.BIND_AUTO_CREATE
        )
    }

    override fun onStop() {
        super.onStop()
        subscriptions.clear()
        if (isServiceConnected) unbindService(this)
        WorkSchedulerUtils.scheduleLocationWork()
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        isServiceConnected = false
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        isServiceConnected = true
    }
}
