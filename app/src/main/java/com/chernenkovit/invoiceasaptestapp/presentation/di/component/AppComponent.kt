package com.chernenkovit.invoiceasaptestapp.presentation.di.component

import com.chernenkovit.invoiceasaptestapp.App
import com.chernenkovit.invoiceasaptestapp.data.di.module.*
import com.chernenkovit.invoiceasaptestapp.data.location.service.LocationService
import com.chernenkovit.invoiceasaptestapp.data.location.service.PeriodicLocationWorker
import com.chernenkovit.invoiceasaptestapp.presentation.MainActivity
import com.chernenkovit.invoiceasaptestapp.presentation.di.module.ContextModule
import com.chernenkovit.invoiceasaptestapp.presentation.di.module.ViewModelModule
import com.chernenkovit.invoiceasaptestapp.presentation.ui.InvoicesFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [ContextModule::class, NetworkModule::class, ExecutorsModule::class,
        ViewModelModule::class, LocationModule::class, DatabaseModule::class, RepositoryModule::class]
)
interface AppComponent {

    fun inject(application: App)
    fun inject(service: LocationService)
    fun inject(worker: PeriodicLocationWorker)
    fun inject(activity: MainActivity)
    fun inject(invoicesFragment: InvoicesFragment)
}