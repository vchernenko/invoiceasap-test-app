package com.chernenkovit.invoiceasaptestapp.presentation.di.module

import android.content.Context
import com.chernenkovit.invoiceasaptestapp.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ContextModule(private val application: App) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application

}