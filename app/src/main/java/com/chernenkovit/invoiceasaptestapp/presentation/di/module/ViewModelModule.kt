package com.chernenkovit.invoiceasaptestapp.presentation.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.chernenkovit.invoiceasaptestapp.base.presentation.di.viewmodel.ViewModelFactory
import com.chernenkovit.invoiceasaptestapp.base.presentation.di.viewmodel.ViewModelKey
import com.chernenkovit.invoiceasaptestapp.presentation.ui.InvoicesViewModel
import com.chernenkovit.invoiceasaptestapp.presentation.ui.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InvoicesViewModel::class)
    abstract fun bindInvoicesViewModel(viewModel: InvoicesViewModel): ViewModel
}