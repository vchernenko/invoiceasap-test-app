package com.chernenkovit.invoiceasaptestapp.presentation.ui

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.chernenkovit.invoiceasaptestapp.R
import com.chernenkovit.invoiceasaptestapp.base.presentation.ui.BaseFragment
import kotlinx.android.synthetic.main.initial_fragment.*

/**
 * First screen fragment with a button
 */
class InitialFragment : BaseFragment() {

    override fun layoutId() = R.layout.initial_fragment

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialScreenShowInvoicesBtn.setOnClickListener { findNavController(this).navigate(R.id.invoicesFragment) }
    }
}
