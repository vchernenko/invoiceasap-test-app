package com.chernenkovit.invoiceasaptestapp.presentation.ui

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.chernenkovit.invoiceasaptestapp.R
import com.chernenkovit.invoiceasaptestapp.base.presentation.ui.BaseFragment
import com.chernenkovit.invoiceasaptestapp.base.presentation.ui.Resource
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerWithInvoices
import com.chernenkovit.invoiceasaptestapp.presentation.ui.adapter.InvoicesController
import kotlinx.android.synthetic.main.invoices_fragment.*
import javax.inject.Inject

/**
 * Fragment to show the list with invoices and customers data
 */
class InvoicesFragment : BaseFragment() {

    @Inject
    lateinit var invoicesController: InvoicesController

    private lateinit var viewModel: InvoicesViewModel

    override fun layoutId() = R.layout.invoices_fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(InvoicesViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        invoicesScreenRecyclerView.layoutManager = LinearLayoutManager(context)
        invoicesScreenRecyclerView.adapter = invoicesController.adapter
        viewModel.customersWithInvoicesLiveData.observe(this, Observer {
            observeCustomersWithInvoices(it)
        })
    }

    private fun observeCustomersWithInvoices(resource: Resource<List<CustomerWithInvoices>>) {
        when (resource) {
            is Resource.Loading -> {
                showData(emptyList(), true)
            }
            is Resource.Success -> {
                showData(resource.data, false)
            }
            is Resource.Error -> {
                showData(emptyList(), false)
            }
        }
    }

    private fun showData(list: List<CustomerWithInvoices>?, showInitialProgress: Boolean) {
        invoicesController.setData(list, showInitialProgress)
    }
}
