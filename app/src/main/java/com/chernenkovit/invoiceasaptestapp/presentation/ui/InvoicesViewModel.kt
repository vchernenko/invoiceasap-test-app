package com.chernenkovit.invoiceasaptestapp.presentation.ui

import androidx.lifecycle.MutableLiveData
import com.chernenkovit.invoiceasaptestapp.base.presentation.ui.BaseViewModel
import com.chernenkovit.invoiceasaptestapp.base.presentation.ui.Resource
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerWithInvoices
import com.chernenkovit.invoiceasaptestapp.domain.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * ViewModel for {@link InvoicesFragment}
 */
class InvoicesViewModel @Inject constructor(private val repository: Repository) : BaseViewModel() {

    var customersWithInvoicesLiveData: MutableLiveData<Resource<List<CustomerWithInvoices>>> =
        MutableLiveData()

    init {
        fetchCustomersWithInvoices()
    }

    fun fetchCustomersWithInvoices() {
        subscriptions.add(repository.getInvoices()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { customersWithInvoicesLiveData.value = Resource.Loading(null) }
            .subscribe({ customersWithInvoicesLiveData.value = Resource.Success(it) },
                { customersWithInvoicesLiveData.value = Resource.Error(it, null) })
        )
    }
}

