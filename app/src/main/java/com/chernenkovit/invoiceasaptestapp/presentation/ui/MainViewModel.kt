package com.chernenkovit.invoiceasaptestapp.presentation.ui

import com.chernenkovit.invoiceasaptestapp.base.DeveloperUtils.log
import com.chernenkovit.invoiceasaptestapp.base.presentation.ui.BaseViewModel
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerEntity
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.InvoiceEntity
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.InvoiceEntity.SentStatusClass.Companion.OPENED
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.InvoiceEntity.SentStatusClass.Companion.VIEWED
import com.chernenkovit.invoiceasaptestapp.domain.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * ViewModel for {@link InvoicesFragment}
 */
class MainViewModel @Inject constructor(private val repository: Repository) : BaseViewModel() {

    init {
        initData()
    }

    private fun initData() {
        subscriptions.add(repository.insertCustomers(
            listOf(
                CustomerEntity("1", "Charlie Mayfield"),
                CustomerEntity("2", "Sam Swanson"),
                CustomerEntity("3", "Bradley Garkel")
            )
        )
            .andThen(
                repository.insertInvoices(
                    listOf(
                        InvoiceEntity("1", "3", "l12347", OPENED, "2020-02-15", 15000, "Paid"),
                        InvoiceEntity("2", "1", "l12345", null, "2020-01-13", 20000, "Late"),
                        InvoiceEntity("3", "2", "l12346", VIEWED, "2020-02-05", 37500, "Open")
                    )
                )
            )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({}, { log(it) })
        )
    }
}