package com.chernenkovit.invoiceasaptestapp.presentation.ui.adapter

import com.airbnb.epoxy.Typed2EpoxyController
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerWithInvoices
import com.chernenkovit.invoiceasaptestapp.presentation.ui.adapter.holder.EmptyListAdapterModel_
import com.chernenkovit.invoiceasaptestapp.presentation.ui.adapter.holder.InvoiceAdapterModel_
import com.chernenkovit.invoiceasaptestapp.presentation.ui.adapter.holder.ProgressFullAdapterModel_
import com.chernenkovit.invoiceasaptestapp.presentation.ui.adapter.holder.SeparatorFullWidthAdapterModel_
import javax.inject.Inject
import kotlin.random.Random.Default.nextInt

/**
 * Main controller (adapter) for populating data in the list
 */
class InvoicesController @Inject constructor() :
    Typed2EpoxyController<List<CustomerWithInvoices>, Boolean>() {

    private var itemsList = mutableListOf<CustomerWithInvoices>()

    override fun buildModels(items: List<CustomerWithInvoices>, showInitialProgress: Boolean) {
        itemsList.addAll(items)
        addInitialProgress(showInitialProgress)
        items.forEach {
            addInvoice(it)
            addSeparator(it)
        }
        addEmptyView(showInitialProgress)
    }

    private fun addInitialProgress(showInitialProgress: Boolean) {
        ProgressFullAdapterModel_()
            .id(nextInt())
            .addIf(
                itemsList.isNullOrEmpty() && showInitialProgress,
                this
            )
    }

    private fun addInvoice(customerWithInvoices: CustomerWithInvoices) {
        customerWithInvoices.invoices.forEach {
            InvoiceAdapterModel_()
                .id(it.id)
                .name(customerWithInvoices.customer.name)
                .number(it.number)
                .date(it.dueDate)
                .balance(it.balance)
                .sentStatus(it.sentStatus)
                .paymentStatus(it.paymentStatus)
                .addTo(this)
        }
    }

    private fun addSeparator(invoice: CustomerWithInvoices) {
        SeparatorFullWidthAdapterModel_()
            .id(invoice.customer.id)
            .addTo(this)
    }

    private fun addEmptyView(showInitialProgress: Boolean) {
        EmptyListAdapterModel_()
            .id(nextInt())
            .addIf(itemsList.isNullOrEmpty() && !showInitialProgress, this)
    }

}