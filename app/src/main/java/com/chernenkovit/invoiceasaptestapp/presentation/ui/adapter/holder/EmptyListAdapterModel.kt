package com.chernenkovit.invoiceasaptestapp.presentation.ui.adapter.holder

import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.chernenkovit.invoiceasaptestapp.R
import com.chernenkovit.invoiceasaptestapp.base.presentation.ui.adapter.holder.BaseHolder

/**
 * Adapter model that is used in case of no data or error
 */
@EpoxyModelClass
abstract class EmptyListAdapterModel : EpoxyModelWithHolder<EmptyViewModelHolder>() {

    override fun getDefaultLayout(): Int = R.layout.view_empty_list_model

    override fun bind(holder: EmptyViewModelHolder) {
    }
}

//add and customize attributes if necessary
class EmptyViewModelHolder : BaseHolder() {
}

