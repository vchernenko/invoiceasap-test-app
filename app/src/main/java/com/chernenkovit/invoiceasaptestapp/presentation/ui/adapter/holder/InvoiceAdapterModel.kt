package com.chernenkovit.invoiceasaptestapp.presentation.ui.adapter.holder

import android.graphics.Color.*
import androidx.appcompat.widget.AppCompatTextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.chernenkovit.invoiceasaptestapp.R
import com.chernenkovit.invoiceasaptestapp.base.presentation.DateUtils.formatServerStringDateToUiStringDate
import com.chernenkovit.invoiceasaptestapp.base.presentation.NumberUtils.formatToCurrency
import com.chernenkovit.invoiceasaptestapp.base.presentation.ui.adapter.holder.BaseHolder
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.InvoiceEntity.PaymentStatusClass.Companion.LATE
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.InvoiceEntity.PaymentStatusClass.Companion.OPEN
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.InvoiceEntity.PaymentStatusClass.Companion.PAID
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.InvoiceEntity.SentStatusClass.Companion.OPENED
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.InvoiceEntity.SentStatusClass.Companion.VIEWED

/**
 * Adapter model that is used show info about customers and invoices
 */
@EpoxyModelClass
abstract class InvoiceAdapterModel : EpoxyModelWithHolder<InvoiceAdapterModelHolder>() {
    @EpoxyAttribute
    lateinit var name: String
    @EpoxyAttribute
    lateinit var number: String
    @EpoxyAttribute
    lateinit var date: String
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var paymentStatus: String? = null
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var balance: Long = 0L
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var sentStatus: String? = null

    override fun getDefaultLayout(): Int = R.layout.view_invoice_model

    override fun bind(holder: InvoiceAdapterModelHolder) {
        with(holder) {
            nameView.text = name
            numberView.text = number
            dateView.text = formatServerStringDateToUiStringDate(date)
            balanceView.text = formatToCurrency(balance)
            sentStatusView.text = sentStatus
            formatSentStatusView(holder)
            paymentStatusView.text = paymentStatus
            formatPaymentStatusView(holder)
        }
    }

    private fun formatSentStatusView(holder: InvoiceAdapterModelHolder) {
        when (sentStatus) {
            OPENED -> {
                holder.sentStatusView.setTextColor(
                    holder.sentStatusView.resources.getColor(
                        R.color.secondary_text
                    )
                )
            }
            VIEWED -> {
                holder.sentStatusView.setTextColor(BLUE)
            }
        }
    }

    private fun formatPaymentStatusView(holder: InvoiceAdapterModelHolder) {
        when (paymentStatus) {
            LATE -> {
                holder.paymentStatusView.setBackgroundColor(RED)
            }
            OPEN -> {
                holder.paymentStatusView.setBackgroundColor(BLUE)
            }
            PAID -> {
                holder.paymentStatusView.setBackgroundColor(CYAN)
            }
        }
    }
}

class InvoiceAdapterModelHolder : BaseHolder() {
    val nameView by bind<AppCompatTextView>(R.id.invoiceItemViewName)
    val numberView by bind<AppCompatTextView>(R.id.invoiceItemViewNumber)
    val dateView by bind<AppCompatTextView>(R.id.invoiceItemViewDate)
    val paymentStatusView by bind<AppCompatTextView>(R.id.invoiceItemViewPaymentStatus)
    val balanceView by bind<AppCompatTextView>(R.id.invoiceItemViewBalance)
    val sentStatusView by bind<AppCompatTextView>(R.id.invoiceItemViewSentStatus)
}