package com.chernenkovit.invoiceasaptestapp.presentation.ui.adapter.holder

import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.chernenkovit.invoiceasaptestapp.R
import com.chernenkovit.invoiceasaptestapp.base.presentation.ui.adapter.holder.BaseHolder

/**
 * Adapter model that is used to show progress bar on data loading
 */
@EpoxyModelClass
abstract class ProgressFullAdapterModel : EpoxyModelWithHolder<ProgressFullScreenHolder>() {

    override fun getDefaultLayout(): Int = R.layout.view_progress_bar_full_screen

    override fun bind(holder: ProgressFullScreenHolder) {
    }
}

//add and customize attributes if necessary
class ProgressFullScreenHolder : BaseHolder() {
}

