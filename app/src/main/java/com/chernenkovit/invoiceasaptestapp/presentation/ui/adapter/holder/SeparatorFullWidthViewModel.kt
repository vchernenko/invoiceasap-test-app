package com.chernenkovit.invoiceasaptestapp.presentation.ui.adapter.holder

import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.chernenkovit.invoiceasaptestapp.R
import com.chernenkovit.invoiceasaptestapp.base.presentation.ui.adapter.holder.BaseHolder

/**
 * Adapter model that is used for separator (divider) between items
 */
@EpoxyModelClass
abstract class SeparatorFullWidthAdapterModel : EpoxyModelWithHolder<SeparatorHolder>() {

    override fun getDefaultLayout(): Int = R.layout.view_separator_full_width

    override fun bind(holder: SeparatorHolder) {
    }
}

//add and customize attributes if necessary
class SeparatorHolder : BaseHolder() {
}

