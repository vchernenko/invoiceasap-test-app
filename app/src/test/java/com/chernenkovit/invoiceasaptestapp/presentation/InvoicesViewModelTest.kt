package com.chernenkovit.invoiceasaptestapp.presentation

import com.chernenkovit.invoiceasaptestapp.AndroidTest
import com.chernenkovit.invoiceasaptestapp.RxImmediateSchedulerRule
import com.chernenkovit.invoiceasaptestapp.base.presentation.ui.Resource
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerEntity
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.CustomerWithInvoices
import com.chernenkovit.invoiceasaptestapp.data.cache.entity.InvoiceEntity
import com.chernenkovit.invoiceasaptestapp.domain.Repository
import com.chernenkovit.invoiceasaptestapp.presentation.ui.InvoicesViewModel
import com.nhaarman.mockito_kotlin.given
import io.reactivex.Single
import org.amshove.kluent.`should be`
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock

class InvoicesViewModelTest : AndroidTest() {

    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    @Mock
    private lateinit var repository: Repository

    @Test
    fun `loading invoices should update live data`() {
        val customersWithInvoices = listOf(
            CustomerWithInvoices(
                CustomerEntity("1", "Charlie Mayfield"),
                listOf(InvoiceEntity("2", "1", "l12345", null, "2020-01-13", 20000, "Late"))
            )
        )

        given { repository.getInvoices() }.willReturn(Single.just(customersWithInvoices))

        InvoicesViewModel(repository).customersWithInvoicesLiveData.observeForever {
            when (it) {
                is Resource.Loading -> {
                    it.data `should be` null
                    it.throwable `should be` null
                }
                is Resource.Success -> {
                    it.data?.size `should be` 1
                    it.data?.get(0)?.customer?.id `should be` "1"
                }
            }
        }
    }
}